package com.upwork.cryptocurrency.ui;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.upwork.cryptocurrency.R;
import com.upwork.cryptocurrency.mvp.MainPresenter;
import com.upwork.cryptocurrency.mvp.MainView;
import com.upwork.cryptocurrency.ui.base.BaseActivity;
import com.upwork.cryptocurrency.utils.recycler_view.EndlessScrollListener;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

import static com.upwork.cryptocurrency.mvp.MainPresenter.ORDER_BY_24_HOURS;
import static com.upwork.cryptocurrency.mvp.MainPresenter.ORDER_BY_NUMBER_OF_MARKETS;
import static com.upwork.cryptocurrency.mvp.MainPresenter.ORDER_BY_PRICE;
import static com.upwork.cryptocurrency.mvp.MainPresenter.ORDER_DIRECTION_ASC;
import static com.upwork.cryptocurrency.mvp.MainPresenter.ORDER_DIRECTION_DESC;
import static com.upwork.cryptocurrency.utils.recycler_view.SwipeRefreshUtils.setColorSchemeColors;

@SuppressLint("NonConstantResourceId")
public class MainActivity extends BaseActivity implements MainView {
    @InjectPresenter
    MainPresenter presenter;

    @BindView(R.id.v_skeleton) ViewGroup vSkeleton;
    @BindView(R.id.swipe_refresh_layout) SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.recycler_view) RecyclerView recyclerView;
    @BindView(R.id.rg_filter) RadioGroup rgFilter;
    @BindView(R.id.img_desc_asc) AppCompatImageView imgDescAsc;
    private CryptoExchangeAdapter adapter;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        configureRecyclerView();
        imgDescAsc.setSelected(true); // By default the order is in descending order. Set image's appearance
        presenter.getExchangeCoins(false, false);
        rgFilter.setOnCheckedChangeListener((group, checkedId) -> {
            switch (checkedId) {
                case R.id.rb_market_cap:
                    presenter.setOrderBy(ORDER_BY_NUMBER_OF_MARKETS);
                    break;
                case R.id.rb_price:
                    presenter.setOrderBy(ORDER_BY_PRICE);
                    break;
                case R.id.rb_24_change:
                    presenter.setOrderBy(ORDER_BY_24_HOURS);
                    break;
                default:
                    // Do nothing
                    break;
            }
            presenter.getExchangeCoins(false, false);
        });
    }

    @OnClick(R.id.img_desc_asc)
    void onDescAscClick(AppCompatImageView v) {
        v.setSelected(!v.isSelected()); // Set the image's appearance
        presenter.setOrderDirection(v.isSelected() ? ORDER_DIRECTION_DESC : ORDER_DIRECTION_ASC);
        presenter.getExchangeCoins(false, false);
    }

    private void configureRecyclerView() {
        // Configuring the list of crypto exchanges
        adapter = new CryptoExchangeAdapter();
        adapter.setCtx(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(20);
        recyclerView.setAdapter(adapter);
        // Listener of loading more items
        EndlessScrollListener endlessListener = new EndlessScrollListener(linearLayoutManager) {
            @Override
            public void onRequestMoreItems() {
                presenter.getExchangeCoins(false, true);
            }
        };
        recyclerView.addOnScrollListener(endlessListener);
        setColorSchemeColors(this, swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(() -> {
            endlessListener.resetState();
            presenter.getExchangeCoins(true, false);
        });
    }

    ///////////////////////////////////////////////////////////////////////////
    // MainView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void showLoadingIndicator(boolean show) {
        vSkeleton.setVisibility(show ? View.VISIBLE : View.GONE); // Showing general indicator
    }

    @Override
    public void showRefreshingIndicator(boolean show) {
        swipeRefreshLayout.setRefreshing(show); // Showing indicator when user pulling down to refresh
    }

    @Override
    public void showLoadingItemIndicator(boolean show) {
        recyclerView.post(() -> adapter.setLoading(show)); // Showing loading more progress indicator at the end of adapter item
    }

    @Override
    public void setObject(List<?> list) {
        recyclerView.post(() -> adapter.setList(list));
    }

    @Override
    public void addObject(List<?> list) {
        recyclerView.post(() -> adapter.addList(list));
    }
}