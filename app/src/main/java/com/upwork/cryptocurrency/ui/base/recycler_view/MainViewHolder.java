package com.upwork.cryptocurrency.ui.base.recycler_view;

import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import butterknife.ButterKnife;

public class MainViewHolder extends RecyclerView.ViewHolder {
    public MainViewHolder(View v) {
        super(v);
        ButterKnife.bind(this, v);
    }
}