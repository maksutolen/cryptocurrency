package com.upwork.cryptocurrency.ui.base.recycler_view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.upwork.cryptocurrency.R;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseRViewAdapter extends RecyclerView.Adapter<MainViewHolder> {
    protected static final int PROGRESS_BAR_LAYOUT_ID = R.layout.include_pb_opaque;

    protected OnItemClickListener onItemClickListener;
    protected boolean loading;
    protected List<Object> data;
    protected Context ctx;
    private LayoutInflater mInflater;

    protected BaseRViewAdapter() {
        this.data = new ArrayList<>();
    }

    public void setCtx(Context ctx) {
        this.ctx = ctx;
        mInflater = LayoutInflater.from(ctx);
    }

    protected abstract int getLayoutResourceId();

    @Override
    public int getItemCount() {
        int count = data.size();
        if (loading)
            count += 1;
        return count;
    }

    @Override
    public int getItemViewType(int position) {
        if (loading && position == getItemCount() - 1) return PROGRESS_BAR_LAYOUT_ID;
        else return getLayoutResourceId();
    }

    public static class SimpleViewHolder extends MainViewHolder {
        public SimpleViewHolder(View v) {
            super(v);
        }
    }

    public void setLoading(boolean loading) {
        if (this.loading && !loading) {
            this.loading = false;
            notifyItemRemoved(getItemCount());
        } else if (!this.loading && loading) {
            this.loading = true;
            notifyItemInserted(getItemCount() - 1);
        }
    }

    public boolean isLoading() {
        return loading;
    }

    public void setList(List<?> newItems) {
        data.clear();
        data.addAll(newItems);
        notifyDataSetChanged();
    }

    public void addList(List<?> newItems) {
        int positionStart = data.size();
        int itemCount = newItems.size();
        data.addAll(newItems);
        notifyItemRangeInserted(positionStart, itemCount);
    }

    public void clearItems() {
        data.clear();
        notifyDataSetChanged();
    }

    protected View inflate(ViewGroup parent, int resource) {
        return mInflater.inflate(resource, parent, false);
    }

    protected IllegalStateException incorrectOnCreateViewHolder() {
        return new IllegalStateException("Incorrect ViewType found");
    }

    protected IllegalStateException incorrectGetItemViewType() {
        return new IllegalStateException("Incorrect object added");
    }

    public interface OnItemClickListener {
        void onItemClick(long id, int index, String title);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
}