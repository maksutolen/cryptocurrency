package com.upwork.cryptocurrency.ui.base;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.upwork.cryptocurrency.R;
import com.upwork.cryptocurrency.mvp.base.BaseMvpView;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.upwork.cryptocurrency.utils.SnackBarUtils.showSnackBar;

public abstract class BaseActivity extends MvpAppCompatActivity implements BaseMvpView {
    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.cl_parent)
    protected ConstraintLayout clParent;

    protected abstract int getLayoutResourceId();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getLayoutResourceId() != -1)
            setContentView(getLayoutResourceId());
        ButterKnife.bind(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem i) {
        if (i.getItemId() == android.R.id.home)
            finish();
        return super.onOptionsItemSelected(i);
    }

    /////////////////////////////////////////////////////////
    // BaseMvpView implementation
    /////////////////////////////////////////////////////////

    @Override
    public void showRequestUnknownError(String error) {
        showSnackBar(clParent, error);
    }

    @Override
    public void showRequestUnknownError(int errorRes) {
        showSnackBar(clParent, getString(errorRes));
    }

    @Override
    public void showConnectionError() {
        showSnackBar(clParent, getString(R.string.connection_error));
    }

    @Override
    public void showNotFoundView(boolean show) {
        if (show)
            showSnackBar(clParent, getString(R.string.no_data));
    }
}