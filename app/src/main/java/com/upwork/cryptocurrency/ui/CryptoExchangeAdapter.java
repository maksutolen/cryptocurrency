package com.upwork.cryptocurrency.ui;

import android.annotation.SuppressLint;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;

import com.upwork.cryptocurrency.R;
import com.upwork.cryptocurrency.data.models.Coin;
import com.upwork.cryptocurrency.ui.base.recycler_view.BaseRViewAdapter;
import com.upwork.cryptocurrency.ui.base.recycler_view.MainViewHolder;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.upwork.cryptocurrency.utils.PriceFormatterUtils.getBTCFormat;
import static com.upwork.cryptocurrency.utils.PriceFormatterUtils.getMoneyFormat;
import static com.upwork.cryptocurrency.utils.SvgImageUtils.loadImage;

public class CryptoExchangeAdapter extends BaseRViewAdapter {
    private static final int LAYOUT_ID = R.layout.adapter_crypto_exchange;

    @Override
    protected int getLayoutResourceId() {
        return LAYOUT_ID;
    }

    @NonNull
    @Override
    public MainViewHolder onCreateViewHolder(@NonNull ViewGroup p, int vt) {
        switch (vt) {
            case LAYOUT_ID:
                return new ViewHolder(inflate(p, LAYOUT_ID));
            case PROGRESS_BAR_LAYOUT_ID:
                return new SimpleViewHolder(inflate(p, PROGRESS_BAR_LAYOUT_ID));
            default:
                throw incorrectOnCreateViewHolder();
        }
    }

    @Override
    public void onBindViewHolder(@NonNull MainViewHolder h, int p) {
        if (h.getItemViewType() == LAYOUT_ID)
            ((ViewHolder) h).bind((Coin) data.get(p));
    }

    @SuppressLint("NonConstantResourceId")
    class ViewHolder extends MainViewHolder {
        @BindView(R.id.img_crypto) AppCompatImageView imgCrypto;
        @BindView(R.id.tv_crypto_name) TextView tvCryptoName;
        @BindView(R.id.tv_crypto_symbol) TextView tvCryptoSymbol;
        @BindView(R.id.tv_price) TextView tvPrice;
        @BindView(R.id.tv_btc_price) TextView tvBtcPrice;
        @BindView(R.id.tv_24_change) TextView tv24Change;

        ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

        public void bind(Coin coin) {
            if (coin == null)
                return;
            loadImage(ctx, imgCrypto, coin.getIconUrl());
            tvCryptoName.setText(coin.getName());
            tvCryptoSymbol.setText(coin.getSymbol());
            tvPrice.setText(ctx.getString(R.string.usd_sign, getMoneyFormat(coin.getPrice())));
            tvBtcPrice.setText(getBTCFormat(coin.getBTCPrice()));
            tv24Change.setText(getMoneyFormat(coin.getVolume24h()));
        }
    }
}
