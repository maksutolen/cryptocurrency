package com.upwork.cryptocurrency;

import com.upwork.cryptocurrency.di.AppGraph;
import com.upwork.cryptocurrency.di.modules.AppModule;
import com.upwork.cryptocurrency.di.modules.network.RepositoryModule;

import javax.inject.Singleton;
import dagger.Component;

@Singleton
@Component(modules = {
        AppModule.class,
        RepositoryModule.class
})
public interface AppComponent extends AppGraph {
    final class Initializer {
        private Initializer() {
            throw new IllegalStateException("AppComponent");
        }

        static AppGraph init(final CryptoCurrencyApplication app) {
            return DaggerAppComponent.builder()
                    .appModule(new AppModule(app))
                    .build();
        }
    }
}