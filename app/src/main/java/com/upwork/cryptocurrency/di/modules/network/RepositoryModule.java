package com.upwork.cryptocurrency.di.modules.network;

import com.upwork.cryptocurrency.data.network.api.API;
import com.upwork.cryptocurrency.data.repository.api.APIRepository;
import com.upwork.cryptocurrency.data.repository.api.APIRepositoryImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(includes = {ApiModule.class})
public class RepositoryModule {
    @Provides
    @Singleton
    APIRepository provideApiRepository(API api) {
        return new APIRepositoryImpl(api);
    }
}