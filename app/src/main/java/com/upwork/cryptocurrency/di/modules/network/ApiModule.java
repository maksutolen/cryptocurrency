package com.upwork.cryptocurrency.di.modules.network;

import com.upwork.cryptocurrency.data.network.api.API;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

import static com.upwork.cryptocurrency.config.CryptoCurrencyConfig.API_BASE_URL;

@Module(includes = {RetrofitModule.class})
public class ApiModule {
    @Provides
    @Singleton
    API provideAPI(Retrofit.Builder builder, OkHttpClient okHttpClient) {
        return builder
                .baseUrl(API_BASE_URL)
                .client(okHttpClient)
                .build()
                .create(API.class);
    }
}