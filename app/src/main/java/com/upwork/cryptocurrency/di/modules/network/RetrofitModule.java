package com.upwork.cryptocurrency.di.modules.network;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.upwork.cryptocurrency.di.modules.AppModule;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.upwork.cryptocurrency.config.CryptoCurrencyConfig.X_ACCESS_TOKEN;
import static com.upwork.cryptocurrency.config.CryptoCurrencyConfig.isDevBuild;
import static java.util.concurrent.TimeUnit.SECONDS;

@Module(includes = {AppModule.class})
public class RetrofitModule {
    private static final int TIMEOUT_IN_SECONDS = 60;

    @Provides
    @Singleton
    Retrofit.Builder provideRetrofitBuilder(Converter.Factory converterFactory, RxJavaCallAdapterFactory adapterFactory) {
        return new Retrofit.Builder()
                .addCallAdapterFactory(adapterFactory)
                .addConverterFactory(converterFactory);
    }

    @Provides
    @Singleton
    Converter.Factory provideConverterFactory(Gson gson) {
        return GsonConverterFactory.create(gson);
    }

    @Provides
    @Singleton
    RxJavaCallAdapterFactory provideAdapterFactory() {
        return RxJavaCallAdapterFactory.create();
    }

    @Provides
    @Singleton
    Gson provideGson() {
        return new GsonBuilder()
                .setPrettyPrinting()
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                .create();
    }

    @Provides
    @Singleton
    OkHttpClient.Builder provideOkHttpClientBuilder(HttpLoggingInterceptor loggingInterceptor,
                                                    StethoInterceptor stethoInterceptor) {
        OkHttpClient.Builder okBuilder = new OkHttpClient.Builder()
                .connectTimeout(TIMEOUT_IN_SECONDS, SECONDS)
                .readTimeout(TIMEOUT_IN_SECONDS, SECONDS)
                .writeTimeout(TIMEOUT_IN_SECONDS, SECONDS)
                .addNetworkInterceptor(chain -> {
                    Request originalRequest = chain.request();
                    Request.Builder rb = originalRequest.newBuilder();
                    rb.header("Accept", "application/json");
                    rb.header("x-access-token", X_ACCESS_TOKEN);
                    rb.method(originalRequest.method(), originalRequest.body());
                    Request newRequest = rb.build();
                    return chain.proceed(newRequest);
                });
        if (isDevBuild()) {
            okBuilder.addInterceptor(loggingInterceptor);
            okBuilder.addNetworkInterceptor(stethoInterceptor);
        }
        return okBuilder;
    }

    @Provides
    @Singleton
    HttpLoggingInterceptor provideHttpLoggingInterceptor() {
        return new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
    }

    @Provides
    @Singleton
    StethoInterceptor provideStethoInterceptor() {
        // If Stetho is enabled, StethoInterceptor allows monitoring network packets in Chrome Dev Tools on your
        // PC through chrome://inspect
        return new StethoInterceptor();
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(OkHttpClient.Builder okHttpClientBuilder) {
        return okHttpClientBuilder.build();
    }
}