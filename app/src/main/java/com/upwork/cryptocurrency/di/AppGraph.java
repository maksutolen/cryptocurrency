package com.upwork.cryptocurrency.di;

import com.upwork.cryptocurrency.mvp.MainPresenter;

public interface AppGraph {
    void inject(MainPresenter p);
}