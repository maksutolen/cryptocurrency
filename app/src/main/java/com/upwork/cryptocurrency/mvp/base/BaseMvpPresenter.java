package com.upwork.cryptocurrency.mvp.base;

import com.arellomobile.mvp.MvpPresenter;
import com.upwork.cryptocurrency.R;
import com.upwork.cryptocurrency.data.network.error.RetrofitErrorHandler;
import com.upwork.cryptocurrency.data.network.exceptions.APIException;
import com.upwork.cryptocurrency.data.network.exceptions.ConnectionTimeOutException;
import com.upwork.cryptocurrency.data.network.exceptions.SocketTimeOutException;
import com.upwork.cryptocurrency.data.network.exceptions.UnknownException;
import com.upwork.cryptocurrency.utils.TLog;

import rx.subscriptions.CompositeSubscription;

public abstract class BaseMvpPresenter<T extends BaseMvpView> extends MvpPresenter<T> {
    protected CompositeSubscription cSub = new CompositeSubscription();

    protected void handleResponseError(Throwable e) {
        StackTraceElement[] ste = e.getStackTrace();
        for (int iterator = 1; iterator <= ste.length; iterator++)
            TLog.e("Err:" + e.toString() +
                    " |Class Name:" + ste[iterator - 1].getClassName() +
                    " |Method Name:" + ste[iterator - 1].getMethodName() +
                    " |Line Number:" + ste[iterator - 1].getLineNumber());
        try {
            RetrofitErrorHandler.handleException(e);
        } catch (APIException apiException) {
            getViewState().showRequestUnknownError(apiException.getMsg());
        } catch (UnknownException e1) {
            getViewState().showRequestUnknownError(R.string.error_occurred);
        } catch (ConnectionTimeOutException | SocketTimeOutException e1) {
            getViewState().showConnectionError();
        }
    }
}