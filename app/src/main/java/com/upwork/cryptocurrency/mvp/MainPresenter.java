package com.upwork.cryptocurrency.mvp;

import com.arellomobile.mvp.InjectViewState;
import com.upwork.cryptocurrency.CryptoCurrencyApplication;
import com.upwork.cryptocurrency.data.models.BaseResponse;
import com.upwork.cryptocurrency.data.models.Exchange;
import com.upwork.cryptocurrency.data.repository.api.APIRepository;
import com.upwork.cryptocurrency.mvp.base.BaseMvpPresenter;

import javax.inject.Inject;

import rx.Subscriber;

@InjectViewState
public class MainPresenter extends BaseMvpPresenter<MainView> {
    public static final String ORDER_BY_24_HOURS = "24hVolume"; // Order by 24h change
    public static final String ORDER_BY_PRICE = "price"; // Order by price
    public static final String ORDER_BY_NUMBER_OF_MARKETS = "numberOfMarkets"; // Order by number of markets
    public static final String ORDER_DIRECTION_DESC = "desc"; // Order in descending order
    public static final String ORDER_DIRECTION_ASC = "asc"; // Order in ascending order
    private static final int ITEMS_LIMIT = 20; // Pagination limit

    // Injecting API repository with Dagger
    @Inject
    APIRepository apiRepository;

    private int pageMin;
    private boolean noMoreItems;
    private boolean isLoading;
    private String orderBy = ORDER_BY_24_HOURS; // by default ordering by 24h change
    private String orderDirection = ORDER_DIRECTION_DESC; // by default order direction is DESC

    public MainPresenter() {
        CryptoCurrencyApplication.getAppGraph().inject(this);
    }

    // Set orderBy value when filter selected by user
    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    // Set orderDirection value when filter selected by user
    public void setOrderDirection(String orderDirection) {
        this.orderDirection = orderDirection;
    }

    // Reset loading more variables
    private void reset() {
        pageMin = 0;
        noMoreItems = false;
        isLoading = false;
    }

    // Showing loading indicators. General, Pull down Refresh, Loading more
    private void showLoading(boolean show, boolean isRefresh, boolean isLoadMore) {
        if (!isRefresh && !isLoadMore) getViewState().showLoadingIndicator(show);
        if (isRefresh) getViewState().showRefreshingIndicator(show);
        if (isLoadMore) getViewState().showLoadingItemIndicator(show);
    }

    // Fetching data from server side using endpoint
    public void getExchangeCoins(boolean isRefresh, boolean isLoadMore) {
        if (isRefresh || !isLoadMore) reset();
        if (noMoreItems || isLoading) return;
        isLoading = true;
        cSub.add(apiRepository.getExchangeCoins(pageMin, ITEMS_LIMIT, orderBy, orderDirection)
                .doOnSubscribe(() -> showLoading(true, isRefresh, isLoadMore))
                .doOnTerminate(() -> showLoading(false, isRefresh, isLoadMore))
                .subscribe(new Subscriber<BaseResponse<Exchange>>() {
                    @Override
                    public void onCompleted() {
                        // Do nothing
                    }

                    @Override
                    public void onError(Throwable e) {
                        handleResponseError(e);
                    }

                    @Override
                    public void onNext(BaseResponse<Exchange> d) {
                        if (d == null || !d.isSuccess()) return;
                        isLoading = false;
                        pageMin = pageMin + ITEMS_LIMIT;
                        Exchange exchange = d.getData();
                        if (exchange == null) return;

                        if (exchange.coins.isEmpty() && isLoadMore)
                            noMoreItems = true;
                        if (!exchange.coins.isEmpty()) {
                            getViewState().showNotFoundView(false); // Show when currency exchanges list is empty
                            if (isRefresh || !isLoadMore)
                                getViewState().setObject(exchange.coins); // In initial setup, set the list of currency exchanges
                            else
                                getViewState().addObject(exchange.coins); // In loading more action, add the list add the bottom of the list
                        } else if (!isLoadMore)
                            getViewState().showNotFoundView(true);
                    }
                }));
    }
}