package com.upwork.cryptocurrency.mvp;

import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.upwork.cryptocurrency.mvp.base.AddToEndSingleByTagStateStrategy;
import com.upwork.cryptocurrency.mvp.base.BaseMvpView;

import java.util.List;

public interface MainView extends BaseMvpView {
    String TAG_LOADING_COMMAND = "loadingCommand";

    @StateStrategyType(value = AddToEndSingleByTagStateStrategy.class, tag = TAG_LOADING_COMMAND)
    void showLoadingIndicator(boolean show);

    void showRefreshingIndicator(boolean show);

    void showLoadingItemIndicator(boolean show);

    void setObject(List<?> list);

    void addObject(List<?> list);
}
