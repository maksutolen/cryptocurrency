package com.upwork.cryptocurrency.mvp.base;

import androidx.annotation.StringRes;

import com.arellomobile.mvp.MvpView;

public interface BaseMvpView extends MvpView {
    void showRequestUnknownError(String error);

    void showRequestUnknownError(@StringRes int errorRes);

    void showConnectionError();

    void showNotFoundView(boolean show);
}