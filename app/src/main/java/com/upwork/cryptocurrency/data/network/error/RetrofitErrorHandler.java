package com.upwork.cryptocurrency.data.network.error;

import com.google.gson.GsonBuilder;
import com.upwork.cryptocurrency.data.network.exceptions.APIException;
import com.upwork.cryptocurrency.data.network.exceptions.ConnectionTimeOutException;
import com.upwork.cryptocurrency.data.network.exceptions.SocketTimeOutException;
import com.upwork.cryptocurrency.data.network.exceptions.UnknownException;
import com.upwork.cryptocurrency.utils.TLog;

import java.io.IOException;
import java.net.SocketTimeoutException;

import retrofit2.Response;
import retrofit2.adapter.rxjava.HttpException;

public class RetrofitErrorHandler {
    private RetrofitErrorHandler() {
        throw new IllegalStateException("RetrofitErrorHandler class");
    }

    public static void handleException(Throwable e) throws
            APIException,
            UnknownException,
            ConnectionTimeOutException, SocketTimeOutException {

        if (e instanceof HttpException) {
            HttpException exception = (HttpException) e;
            Response response = exception.response();
            if (response != null) {
                APIError apiError = null;
                try {
                    apiError = parseError(response);
                } catch (Exception e1) {
                    TLog.e("handleException e=" + e.toString());
                }
                if (apiError != null)
                    throw new APIException(apiError.getMessage());
            }
            throw new UnknownException();
        } else if (e instanceof IOException) {
            throw new ConnectionTimeOutException();
        } else if (e instanceof SocketTimeoutException) {
            throw new SocketTimeOutException();
        } else {
            throw new UnknownException();
        }
    }

    private static APIError parseError(Response<?> response) throws IOException {
        return new GsonBuilder()
                .create().fromJson(response.errorBody().string(), APIError.class);
    }
}