package com.upwork.cryptocurrency.data.network.exceptions;

import static com.upwork.cryptocurrency.utils.PrimitiveTypeUtils.getValidStr;

public class APIException extends Exception {
    private String msg;

    public APIException(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return getValidStr(msg);
    }
}