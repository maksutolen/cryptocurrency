package com.upwork.cryptocurrency.data.repository.api;

import com.upwork.cryptocurrency.data.models.BaseResponse;
import com.upwork.cryptocurrency.data.models.Exchange;

import rx.Observable;

public interface APIRepository {
    Observable<BaseResponse<Exchange>> getExchangeCoins(int offset, int limit,
                                                        String orderBy, String orderDirection);
}