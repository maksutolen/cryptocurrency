package com.upwork.cryptocurrency.data.models;

import static com.upwork.cryptocurrency.utils.PrimitiveTypeUtils.getValidStr;

public class BaseResponse<T> {
    private static final String STATUS_SUCCESS = "success";
    private T data;
    private String status;

    public T getData() {
        return data;
    }

    public boolean isSuccess() {
        return getValidStr(status).equals(STATUS_SUCCESS);
    }
}