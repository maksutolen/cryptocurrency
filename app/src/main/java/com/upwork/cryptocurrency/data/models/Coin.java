package com.upwork.cryptocurrency.data.models;

import com.google.gson.annotations.SerializedName;

import static com.upwork.cryptocurrency.utils.PrimitiveTypeUtils.getValidDouble;
import static com.upwork.cryptocurrency.utils.PrimitiveTypeUtils.getValidStr;

public class Coin {
    public String symbol;
    public String name;
    public String iconUrl;
    public Double price;
    public Double btcPrice;
    @SerializedName("24hVolume")
    public Double volume24h;

    public String getName() {
        return getValidStr(name);
    }

    public String getSymbol() {
        return getValidStr(symbol);
    }

    public String getIconUrl() {
        return getValidStr(iconUrl);
    }

    public double getPrice() {
        return getValidDouble(price);
    }

    public double getBTCPrice() {
        return getValidDouble(btcPrice);
    }

    public double getVolume24h() {
        return getValidDouble(volume24h);
    }
}