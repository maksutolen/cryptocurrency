package com.upwork.cryptocurrency.data.network.error;

import static com.upwork.cryptocurrency.utils.PrimitiveTypeUtils.getValidStr;

public class APIError {
    private String status;
    private String message;

    public String getStatus() {
        return getValidStr(status);
    }

    public String getMessage() {
        return getValidStr(message);
    }
}