package com.upwork.cryptocurrency.data.repository.api;

import com.upwork.cryptocurrency.data.models.BaseResponse;
import com.upwork.cryptocurrency.data.models.Exchange;
import com.upwork.cryptocurrency.data.network.api.API;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class APIRepositoryImpl implements APIRepository {
    private final API api;
    private static final String UIID = "-zdvbieRdZ";

    public APIRepositoryImpl(API api) {
        this.api = api;
    }

    @Override
    public Observable<BaseResponse<Exchange>> getExchangeCoins(int offset, int limit,
                                                               String orderBy, String orderDirection) {
        return api.getExchangeCoins(UIID, offset, limit, orderBy, orderDirection)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}