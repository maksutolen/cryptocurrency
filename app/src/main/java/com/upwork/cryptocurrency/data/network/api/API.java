package com.upwork.cryptocurrency.data.network.api;

import com.upwork.cryptocurrency.data.models.BaseResponse;
import com.upwork.cryptocurrency.data.models.Exchange;

import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

public interface API {
    /**
     * @param uuid           UUID of reference currency, which rate is used to calculate the volume. Defaults to US Dollar.
     * @param limit          Used for pagination. Default value is 50
     * @param offset         Used for pagination. Default value is 0
     * @param orderBy        Index to sort on. Default value is {@link com.upwork.cryptocurrency.mvp.MainPresenter.ORDER_BY_24_HOURS}
     * @param orderDirection order in ascending or descending order. Default value is {@link com.upwork.cryptocurrency.mvp.MainPresenter.ORDER_DIRECTION_DESC}
     * @return observable of exchange coins
     */
    @GET("exchange/{uuid}/coins")
    Observable<BaseResponse<Exchange>> getExchangeCoins(@Path("uuid") String uuid,
                                                        @Query("offset") int offset,
                                                        @Query("limit") int limit,
                                                        @Query("orderBy") String orderBy,
                                                        @Query("orderDirection") String orderDirection);
}