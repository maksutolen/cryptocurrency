package com.upwork.cryptocurrency.config;

import com.upwork.cryptocurrency.BuildConfig;

public class CryptoCurrencyConfig {
    private CryptoCurrencyConfig() {
        throw new IllegalStateException("CryptoCurrencyConfig class");
    }

    // Base url
    public static final String SCHEME = "https://";
    public static final String DOMAIN = BuildConfig.URL_BASE;
    public static final String API_BASE_URL = SCHEME + DOMAIN + "v2/";
    public static final String X_ACCESS_TOKEN = BuildConfig.X_ACCESS_TOKEN;

    public static boolean isDevBuild() {
        return BuildConfig.DEBUG;
    }
}