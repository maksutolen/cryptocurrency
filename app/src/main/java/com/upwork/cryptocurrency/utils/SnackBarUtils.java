package com.upwork.cryptocurrency.utils;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.StringRes;

import com.google.android.material.snackbar.Snackbar;
import com.upwork.cryptocurrency.R;

public class SnackBarUtils {
    private static final int DEFAULT_MAX_LINES = 5;

    public static Snackbar multilineSnackBar(View view, @StringRes int resId, int duration) {
        return multilineSnackBar(view, resId, duration, -1);
    }

    public static Snackbar multilineSnackBar(View view, CharSequence text, int duration) {
        return multilineSnackBar(view, text, duration, -1);
    }

    public static Snackbar multilineSnackBar(View view, @StringRes int resId, int duration, int maxLines) {
        return makeMultiline(Snackbar.make(view, resId, duration), maxLines);
    }

    public static Snackbar multilineSnackBar(View view, CharSequence text, int duration, int maxLines) {
        return makeMultiline(Snackbar.make(view, text, duration), maxLines);
    }

    private static Snackbar makeMultiline(Snackbar snackbar, int maxLines) {
        TextView textView = snackbar.getView().findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setMaxLines(maxLines > 0 ? maxLines : DEFAULT_MAX_LINES);
        return snackbar;
    }

    public static void showSnackBar(View parent, int stringId) {
        final Snackbar snackbar = SnackBarUtils.multilineSnackBar(parent, stringId, Snackbar.LENGTH_LONG);
        snackbar.setAction(R.string.dismiss, view -> snackbar.dismiss());
        snackbar.show();
    }

    public static void showSnackBar(View parent, String s) {
        final Snackbar snackbar = SnackBarUtils.multilineSnackBar(parent, s, Snackbar.LENGTH_LONG);
        snackbar.setAction(R.string.dismiss, view -> snackbar.dismiss());
        snackbar.show();
    }
}