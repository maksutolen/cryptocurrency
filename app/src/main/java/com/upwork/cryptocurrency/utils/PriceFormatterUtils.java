package com.upwork.cryptocurrency.utils;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

public class PriceFormatterUtils {
    private PriceFormatterUtils() {
        throw new IllegalStateException("PriceFormatterUtils class");
    }

    public static String getMoneyFormat(double money) {
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.UK);
        DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
        symbols.setGroupingSeparator(',');
        symbols.setDecimalSeparator('.');
        formatter.setMaximumFractionDigits(2);
        formatter.setMinimumFractionDigits(2);
        formatter.setDecimalFormatSymbols(symbols);
        formatter.setRoundingMode(RoundingMode.FLOOR);
        return formatter.format(Double.valueOf(String.valueOf(money).trim().replace(",", "")));
    }

    public static String getBTCFormat(double price) {
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.UK);
        DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
        symbols.setGroupingSeparator(',');
        symbols.setDecimalSeparator('.');
        formatter.setMaximumFractionDigits(8);
        formatter.setMinimumFractionDigits(8);
        formatter.setDecimalFormatSymbols(symbols);
        formatter.setRoundingMode(RoundingMode.CEILING);
        return formatter.format(Double.valueOf(String.valueOf(price).trim().replace(",", "")));
    }
}