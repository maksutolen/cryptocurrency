package com.upwork.cryptocurrency.utils;

import android.content.Context;

import androidx.core.content.ContextCompat;

import com.upwork.cryptocurrency.R;

public class ColorUtils {
    private ColorUtils() {
        throw new IllegalStateException("Color Utility class");
    }

    public static int[] getColorsForSwipeRefreshLayout(Context context) {
        return new int[]{
                ContextCompat.getColor(context, R.color.colorPrimary),
                ContextCompat.getColor(context, R.color.colorPrimaryDark),
                ContextCompat.getColor(context, R.color.colorAccent)
        };
    }
}
