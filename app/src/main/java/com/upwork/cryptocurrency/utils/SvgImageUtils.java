package com.upwork.cryptocurrency.utils;

import android.app.Activity;
import android.content.Context;

import androidx.appcompat.widget.AppCompatImageView;

import com.ahmadrosid.svgloader.SvgLoader;

public class SvgImageUtils {
    private SvgImageUtils() {
        throw new IllegalStateException("SvgImageUtils class");
    }

    public static void loadImage(Context context, AppCompatImageView view, String url) {
        SvgLoader.pluck()
                .with((Activity) context)
                .load(url, view);
    }
}