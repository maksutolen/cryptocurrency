package com.upwork.cryptocurrency.utils.recycler_view;

import android.content.Context;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import static com.upwork.cryptocurrency.utils.ColorUtils.getColorsForSwipeRefreshLayout;

public class SwipeRefreshUtils {
    public static void setColorSchemeColors(Context context, SwipeRefreshLayout swipeRefreshLayout) {
        swipeRefreshLayout.setColorSchemeColors(getColorsForSwipeRefreshLayout(context));
    }

    public static void showSwipeRefreshLayout(final SwipeRefreshLayout swipeRefreshLayout) {
        if (swipeRefreshLayout != null)
            swipeRefreshLayout.post(() -> swipeRefreshLayout.setRefreshing(true));
    }

    public static void hideSwipeRefreshLayout(final SwipeRefreshLayout swipeRefreshLayout) {
        if (swipeRefreshLayout != null)
            swipeRefreshLayout.post(() -> swipeRefreshLayout.setRefreshing(false));
    }
}
