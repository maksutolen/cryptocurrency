package com.upwork.cryptocurrency.utils;

import android.util.Log;

import static com.upwork.cryptocurrency.config.CryptoCurrencyConfig.isDevBuild;

public class TLog {
    private static final String TAG = "TLog";

    private TLog() {
        throw new IllegalStateException("TLog class");
    }

    public static void e(String message) {
        if (isDevBuild())
            Log.e(TAG, message);
    }

    public static void d(String message) {
        if (isDevBuild())
            Log.d(TAG, message);
    }
}