package com.upwork.cryptocurrency.utils;

import java.util.List;

public class PrimitiveTypeUtils {
    public static final String EMPTY_STR = "";

    public static String trim(String s) {
        return s != null ? s.trim() : null;
    }

    public static int length(String s) {
        return s == null ? 0 : s.length();
    }

    public static boolean isStringOk(String text) {
        return text != null && text.trim().length() > 0;
    }

    public static String getValidStr(String text) {
        return isStringOk(text) ? text : EMPTY_STR;
    }

    public static boolean isIntOk(Integer number) {
        return number != null && number > 0;
    }

    public static int getValidInt(Integer number) {
        return number != null ? number : 0;
    }

    public static double getValidDouble(Double number) {
        return number != null ? number : 0;
    }

    public static boolean getValidBool(Boolean bool) {
        return bool != null ? bool : false;
    }

    public static float getValidFloat(Float number) {
        return number != null ? number : 0;
    }

    public static long getValidLong(Long number) {
        return number != null ? number : 0;
    }

    public static boolean isListOk(List<?> list) {
        return (list != null && !list.isEmpty());
    }

    public static boolean isNumeric(String str) {
        try {
            Integer.parseInt(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}