package com.upwork.cryptocurrency;

import android.app.Application;

import com.facebook.stetho.Stetho;
import com.upwork.cryptocurrency.di.AppGraph;

import static com.upwork.cryptocurrency.config.CryptoCurrencyConfig.isDevBuild;

public class CryptoCurrencyApplication extends Application {
    private static AppGraph appGraph;

    @Override
    public void onCreate() {
        super.onCreate();
        appGraph = AppComponent.Initializer.init(this);
        if (isDevBuild())
            Stetho.initializeWithDefaults(this);
    }

    public static AppGraph getAppGraph() {
        return appGraph;
    }
}